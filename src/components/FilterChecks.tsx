import React, { FC } from 'react'
import { keys, propertieKeys } from '../@types/person';
import './FilterChecks.css';

interface Props {
    arrayForProperties: propertieKeys,
    setArrayForProperties: React.Dispatch<React.SetStateAction<propertieKeys>>,
    setSearchBy: React.Dispatch<React.SetStateAction<keys[]>>
}
export const FilterChecks: FC<Props> = ({arrayForProperties, setSearchBy, setArrayForProperties}) => {
  return (
    <>
        {Object.keys(arrayForProperties).map((property)=>{
            const objectIterable = arrayForProperties[property as keys];
            return(
            <div className="checksProp" key={`key_${objectIterable.name}`}>
                <input
                type="checkbox"
                name={objectIterable.name}
                id={`id_${objectIterable.name}`}
                value={objectIterable.name}
                onChange={(e)=>{
                    (e.target.checked)
                    ? setSearchBy(prevSearch => [...prevSearch, e.target.name as keys])
                    : setSearchBy(prevSearch => prevSearch.filter(p => p != e.target.name));
                    setArrayForProperties(prevState => ({
                    ...prevState,
                    [e.target.name]: {
                        ...prevState[e.target.name as keys],
                        isChecked: e.target.checked
                    }
                    }));
                }}
                />
                <label htmlFor={`id_${objectIterable.name}`}>{objectIterable.name.replaceAll('_',' ')}</label>
            </div>
            );
        })}
    </>
  )
}
