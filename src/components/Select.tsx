import React from 'react'
import './Select.css'

export default function Select() {

    const dataSource = [
        { name: 'people' },
        { name: 'randomData' },
    ]
    return (
        <select name='selectData' className='selectData'>
            <option>Elije una Opción</option>
            {dataSource.map((data)=>(
                <option key={data.name} value={data.name}>{data.name}</option>
            ))}
        </select>
    )
}
