import React from 'react'
import './InputSearch.css'

interface Props {
    query: string,
    setQuery: (value: string) => void;
}

export const InputSearch = (props: Props) => {
  return (
    <>
        <input className='InputSearch' type='text' placeholder='Ingresa una busqueda' value={props.query} onChange={(e) => { props.setQuery(e.target.value) }} />
    </>
  )
}
