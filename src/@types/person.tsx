export type keys = 
'id' |
'first_name' |
'last_name' |
'email' |
'gender' |
'ip_address';

export interface checkInputProps {
    isChecked: boolean,
    name: string
}

export type propertieKeys = Record<keys, checkInputProps>