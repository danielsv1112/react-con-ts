import { useState } from 'react'
import './App.css'
import { InputSearch } from './components/InputSearch'
import Select from './components/Select'
import { people, person } from './data/people'
import genericSearch from './utilities/genericSearch'
import { keys } from './@types/person';
import { FilterChecks } from './components/FilterChecks'

function App() {
  const [query, setQuery] = useState('');
  // const propertiesArray: Array<properties> = Object.keys(people[0]) as Array<properties>;
  const [arrayForProperties, setArrayForProperties] = useState(
    {
      id: { isChecked: false, name: 'id'},
      first_name: { isChecked: false, name: 'first_name'},
      last_name: { isChecked: false, name: 'last_name'},
      email: { isChecked: false, name: 'email'},
      gender: { isChecked: false, name: 'gender'},
      ip_address: { isChecked: false, name: 'ip_address'}
    }
  );
  const [searchBy, setSearchBy] = useState<Array<keys>>([])

  return (
    <div className="App">
      <section className='colFilter'>
        <div className="filaInput">
          <Select />
        </div>
        <div className="filaInput">
          <InputSearch query={query} setQuery={setQuery}/>
        </div>
        <div className="filaInput">
          <FilterChecks arrayForProperties={arrayForProperties} setSearchBy={setSearchBy} setArrayForProperties={setArrayForProperties}/>
        </div>
      </section>
      <section className='colResult'>
        {
          people.filter(person => genericSearch(person, searchBy, query)).map(person => {
            return (
              <p key={person.id}>{person.first_name} {person.last_name}</p>
            )
          })
        }
      </section>
    </div>
  )
}

export default App
